#!/bin/bash
#Run the command in master node only
kubeadm init --pod-network-cidr "10.244.0.0/16" --cri-socket "unix:///var/run/cri-dockerd.sock"
kubectl apply -f https://github.com/flannel-io/flannel/releases/latest/download/kube-flannel.yml

#after run above command 
#run this command
mkdir -p "$HOME"/.kube
sudo cp -i /etc/kubernetes/admin.conf "$HOME"/.kube/config
sudo chown "$(id -u)":"$(id -g)" "$HOME"/.kube/config


...............................................................................................................
#after run kubeadm init command you get a token copy the token and paste into worker node
#before paste the toke make sure you are root user
#after paste the token add the cri socket command

#if you are not super user you got this error in last line --------      --v=5
#example
#    token --cri-socket "unix:///var/run/cri-dockerd.sock"

#example
#     kubeadm join 10.52.25.122:6443 --token zoxka1.x31ku2quwo0oz48f \
#                      --discovery-token-ca-cert-hash sha256:add2dcce8f373e83e132dbf5d6917243c312a62c92fe4b984630949a4002e81a --cri-socket "unix:///var/run/cri-dockerd.sock"



#after successful run this command go to the master node and run the command
#kubectl get pod
