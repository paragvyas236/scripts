#!/bin/bash
sudo apt update
sudo apt install software-properties-common
sudo add-apt-repository --yes --update ppa:ansible/ansible
sudo apt install ansible -y
#install boto and boto 3 for ansible 
#when we need to launch new instance from ansible-playbook so we want boto and boto3
sudo apt install python3-pip -y
sudo pip install boto boto3
