#!/bin/bash
date=$(date +"%d/%m/%Y %H:%M:%S")
alert=75

df -H | awk '{print $1 " " $5}' | while read output;
do
    file_system=$(echo $output | awk '{print $1}')
    usage=$(echo $output | awk '{print $2}' | cut -d'%' -f1)
        if [ $usage -ge $alert ]
            then
                    echo critical disk: $file_system $usage $date
        fi
done
