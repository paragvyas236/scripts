#!/bin/bash
sudo apt update
sudo apt install openjdk-17-jdk -y
sudo apt install unzip
sudo wget -O /usr/share/keyrings/jenkins-keyring.asc \
  https://pkg.jenkins.io/debian-stable/jenkins.io-2023.key
echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] \
  https://pkg.jenkins.io/debian-stable binary/ | sudo tee \
  /etc/apt/sources.list.d/jenkins.list > /dev/null
sudo apt-get update
sudo apt-get install jenkins -y


# change ownership from root to jenkins
#when we create a jenkins pipeline, jenkins need the permission for run docker and create a containers
sudo systemctl stop docker
sudo systemctl start docker
sudo chown jenkins:jenkins /var/run/docker.sock

#if this 3 command not work, you can do manually also