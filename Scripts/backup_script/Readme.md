# Backup Script

## Overview
This script automates the process of creating backups for a specified project directory, uploading them to Google Drive using rclone, and implementing a rotational backup strategy to manage backup retention. It also sends a cURL request upon successful backup completion.

## Setup
1. **Clone Repository:**
   - Clone the repository locally:
     ```bash
     git clone https://gitlab.com/paragvyas236/pr1.git -b master /home/parag/pro/
     ```

2. **Variables Configuration:**
   - Update the following variables in the script according to your setup:
     - `project_directory`: Path to the project directory.
     - `backup_directory`: Directory where backups will be stored locally.
     - `google_drive_directory`: Google Drive folder to upload backups.

3. **Rotational Backup Settings:**
   - Adjust the values of `DAILY_BACKUPS_RETENTION`, `WEEKLY_BACKUPS_RETENTION`, and `MONTHLY_BACKUPS_RETENTION` variables to define the retention periods for daily, weekly, and monthly backups, respectively.

## Usage
1. **Execute Script:**
   - Run the script using the following command:
     ```bash
     bash backup_script.sh
     ```

## Rotational Backup Strategy
- The script implements a rotational backup strategy to manage backup retention:
  - It deletes daily backups older than the specified retention period (`DAILY_BACKUPS_RETENTION` days).
  - It deletes weekly backups older than the specified retention period (`WEEKLY_BACKUPS_RETENTION * 7` days).
  - It deletes monthly backups older than the specified retention period (`MONTHLY_BACKUPS_RETENTION * 30` days).

## cURL Request
- Upon successful backup completion, the script sends a cURL request with the backup details to a specified URL.
- You can configure the cURL request details in the `send_curl_request()` method.

## Dependencies
- Ensure that `rclone` is installed and configured for Google Drive.

## Notes
- Test the script thoroughly in a testing environment before deploying it to a production environment.
- Make sure you have proper permissions to access the project directory, backup directory, and Google Drive.

---
